# pragma once
# include <iostream>
# include <cmath>
# include "Point.hpp"

using Length = double;
using XDisp = long long;
using YDisp = long long;

class Displacement
{

    public:

        Displacement() = default;

        Displacement(XDisp x, YDisp y) 
            : x(x),
              y(y)
        {}

        Length length() {
            auto x_ = static_cast<double>(x);
            auto y_ = static_cast<double>(y);
            return std::pow((x_*x_) + (y_*y_), 0.5);
        }

        

    private:

        XDisp x{};
        YDisp y{};
};