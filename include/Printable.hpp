# pragma once
# include <iostream>
# include <type_traits>
# include <concepts>

template <typename Type>
concept Primitive = requires {
     typename std::enable_if<std::is_fundamental<Type>::value>::type;
};

template <typename Type>
concept DefaultConstructible = requires {
    { std::is_default_constructible<Type>::type };
};

template <typename Type>
concept PrintableClassType = requires(Type t) {
    { std::cout << t } -> std::same_as<std::ostream&>;
};

template <typename Type>
concept OstreamOperatorOverloaded = requires(Type object) {
    { DefaultConstructible<Type> }; 
    { std::declval<std::ostream>().operator<<(object) };
};

template <typename Type>
concept Printable = requires {
   requires ( ( Primitive<Type> and OstreamOperatorOverloaded<Type> ) or ( PrintableClassType<Type> ) );
};

template <typename Type>
concept InputStreamable = requires(Type object) {
    { std::cin >> object } -> std::is_same<std::istream&>;
};

template <typename Type>
concept Streamable = requires {
    requires ( Printable<Type> and InputStreamable<Type> );
};

