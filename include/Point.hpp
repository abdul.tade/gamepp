# pragma once
# include <iostream>
# include "Displacement.hpp"

using XPoint = long long;
using YPoint = long long;


class Point 
{
    public:

        Point() {}

        Point(const XPoint x, const YPoint y)
            : x(x),
              y(y)
        {}

        long long operator[](const std::size_t indx) {

            if (indx > 1) {
                throw std::out_of_range("index must in range [0-1]");
            }

            return (indx == 0) ? x : y;
        }

        void move(XPoint x, YPoint y) {
            this->x += x;
            this->y += y; 
        }

        

        Displacement operator-(const Point& p) {
            return Displacement{x - p.x, y - p.y};
        }
    
    private:

        XPoint x{};
        YPoint y{};
};