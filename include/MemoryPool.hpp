// Type your code here, or load an example.
# include <iostream>
# include <memory>
# include <mutex>
# include <algorithm>
# include <vector>
# include <utility>
# include <type_traits>
# include "Point.hpp"


template <typename ObjectType>
class MemoryPool
{
    public:

        [[ nodiscard ]] MemoryPool() {}

        [[ nodiscard ]] MemoryPool(MemoryPool&& pool)
            : memoryPool(std::exchange(memoryPool,std::vector<MemData>{}))
        {}

        MemoryPool(const MemoryPool& p) = delete;

        virtual ~MemoryPool() = default;

        MemoryPool operator=(const MemoryPool& pool) = delete;

        MemoryPool operator=(MemoryPool&& pool)
        {
            memoryPool = std::exchange(memoryPool, std::vector<MemData>{});
        }

        template <typename... Args>
        [[ nodiscard ]] ObjectType* construct(Args&&... args) {
            std::lock_guard<std::mutex> lock(mutex);
            auto allocatedObject = findAndConstructObject(std::forward<Args>(args) ...);

            if (allocatedObject == nullptr) {
                allocatedObject = new ObjectType{std::forward<Args>(args)...};
                memoryPool.push_back(MemData{std::shared_ptr<ObjectType>(allocatedObject)});
            }

            return allocatedObject;
        }

        void destroy(ObjectType* objectPtr) {
            std::lock_guard<std::mutex> lock(mutex);
            for (auto& mem : memoryPool) {
                if (mem.pointer.get() == objectPtr) {
                    mem.is_allocated = false;
                    objectPtr = nullptr;
                }
            }
        }

        std::size_t size() const noexcept {
            std::lock_guard<std::mutex> lock(mutex);
            return memoryPool.size();
        }

    private:

        struct MemData {

            public:

                bool is_allocated = true;
                std::shared_ptr<ObjectType> pointer{};

                MemData() = default;

                MemData(std::shared_ptr<ObjectType> pointer)
                    : pointer(pointer)
                {}

                bool operator==(const MemData& m) {
                    return (m.is_allocated == is_allocated) and
                           (m.pointer == pointer);
                }

                bool operator!=(const MemData& m) {
                    return not operator==(m);
                }

        };

        template <typename... Args>
        ObjectType* findAndConstructObject(Args&&... args) {
            auto freeMem = static_cast<ObjectType*>(nullptr);
            
            for (auto& mem : memoryPool) {
                if (not mem.is_allocated) {
                    mem.is_allocated = true;
                    freeMem = new (mem.pointer.get()) ObjectType{std::forward<Args>(args)...}; /* using placement new operator */
                }
            }

            return freeMem;
        }

        std::mutex mutex{};
        std::vector<MemData> memoryPool;
};

int main() 
{
   MemoryPool<Point> pool{};
   auto ptr = pool.construct(10, 10);
   std::cout << ptr->operator[](0);
   
}