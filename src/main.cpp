# include <iostream>
# include "../include/Printable.hpp"

template <Printable T>
struct test {
    void print(const T& t) {
        std::cout << t;
    }
};

int main() {
    test<char> t;
    t.print('a');
}